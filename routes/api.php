<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* Route::get('login','App\Http\Controllers\AuthController@login');
Route::post('singup','App\Http\Controllers\AuthController@singup');
Route::get('logout','App\Http\Controllers\AuthController@logout');
Route::get('user','App\Http\Controllers\AuthController@user');

Route::middleware('api')->post('nuevo/register','App\Http\Controllers\Api\AuthController@register');
Route::middleware('api')->post('nuevo/login','App\Http\Controllers\Api\AuthController@login'); */

//User
Route::get('user/index','App\Http\Controllers\UsersController@index');
Route::post('user/store','App\Http\Controllers\UsersController@store');
Route::get('user/show/{id}','App\Http\Controllers\UsersController@show');
Route::put('user/update/{id}','App\Http\Controllers\UsersController@update');
Route::delete('user/destroy/{id}','App\Http\Controllers\UsersController@destroy');

//Video
Route::get('index','App\Http\Controllers\VideosController@index');
Route::post('store','App\Http\Controllers\VideosController@store');
Route::put('update/{id}','App\Http\Controllers\VideosController@update');
Route::get('show/{id}','App\Http\Controllers\VideosController@show');
Route::delete('destroy/{id}','App\Http\Controllers\VideosController@destroy');

//Comentario
Route::get('comentario/index','App\Http\Controllers\ComentariosController@index');
Route::post('comentario/store','App\Http\Controllers\ComentariosController@store');
Route::put('comentario/update/{id}','App\Http\Controllers\ComentariosController@update');
Route::get('comentario/show/{id}','App\Http\Controllers\ComentariosController@show');
Route::delete('comentario/destroy/{id}','App\Http\Controllers\ComentariosController@destroy');


//Subir Archivos

Route::post('uploadFile','App\Http\Controllers\ArchivoController@uploadFile');

//Route::get('index','App\Http\Controllers\ComentariosController@index');

// Videos de un usuario --> hasMany --> 1:N ()
Route::get("/videoUser/{user_id}", function($user_id){
  //  $videos=App\Models\Video::all()->where("user_id",$user_id);
   // $user=App\Models\Video::find(1)->usuario->name;
   // var_dump($user);
  //  die();
  $videos= "Hola";
    return response()->json($videos);
});

//Usuario de un video  --> belongsTo --> 1:1 ()
Route::get("/userVideo/{id}", function($id){
    $user=App\Models\Video::all()->where("id",$id);
    return response()->json($user);
});

//Comentarios de un usuario --> hasMany --> 1:N (Ok)
Route::get("/commentUser/{user_id}", function($user_id){
$commnets=App\Models\Comentario::all()->where("user_id",$user_id);
return response()->json($commnets);
});

//Comentarips de un video --> hasMany -->1:N
Route::get("commentVideo/{video_id}", function($video_id){
 $commnets=App\Models\Comentario::all()->where("video_id",$video_id);
return response()->json($commnets);
});
//Login y registro
//Route::middleware('api')->post('new/register','App\Http\Controllers\Api\AuthController@register');
//Route::middleware('api')->post('new/login','App\Http\Controllers\Api\AuthController@login');