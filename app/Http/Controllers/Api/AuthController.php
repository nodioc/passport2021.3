<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Video;
use App\Models\Comentario;
use Illuminate\Support\Str;

class AuthController extends Controller
{
public function register(Request $request){
try {
    $validateData=$request->validate([
    'role'=>'required|max:7',
    'name'=>'required|max:70',
    'surname'=>'required|max:70',
    'email'=>'email|required',
    'password'=>'required|confirmed',
    'image'=>'required'
    ]);
    
    $validateData['password']=bcrypt($request->password);
    $validateData['remember_token']=$request['remember_token']=Str::random(10);
   
    
    $user=User::create($validateData);
    $accessToken=$user->createToken('authToken')->accessToken;
   return response(['message'=>$user, 'status'=>'Success','code'=>200, 'access_token'=>$accessToken]);
    
    } catch (\Exception $e) {
   return response()->json(['message'=>'Error!! Invalid Register Data','code'=>400]);
    }
}

public function login(Request $request){
$loginData=$request->validate([
'email'=>'email|required',
'password'=>'required'
]);

if(!auth()->attempt($loginData))
return response()->json(['message'=>'Invalid Credentials','code'=>400]);

$accessToken=auth()->user()->createToken('authToken')->accessToken;
return response(['user'=>auth()->user(), 'access_token'=>$accessToken]);

}

}


