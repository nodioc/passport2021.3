<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class ArchivoController extends Controller
{
    public function uploadFile(Request $request)
    {
        if($request->hasFile('file')){
            $file=$request->file('file');
            $filename=$file->getClientOriginalName();

            $filename =pathinfo($filename, PATHINFO_FILENAME);
            $name_File = str_replace(" " , "_",$filename);

            $extension =$file->getClientOriginalExtension();
            //guardamos la cadena del nombre del archivo
            $picture= date('His') . '-' .  $name_File . '.'.$extension;
            $file->move(public_path('public/'),$picture);

            return response()->json(["mensaje" => "Archivo subido existosamente"]);

        }else{
            return response()->json(["mensaje" => "Archivo NO subido "]);
        }          
    }
}
