<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Video;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video=Video::all();
        return response()->json($video);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'user_id'=>'required|max:6',
            'title'=>'required|max:60',
            'description'=>'required|max:100',
            'status'=>'required|max:10',
            'image'=>'required|max:30',
            'video_path'=>'required|max:50'
        ]);           

        try{
            $video=new Video;
            $video->user_id=$request->input('user_id');
            $video->title=$request->input('title');
            $video->description=$request->input('description');
            $video->status=$request->input('status');
            $video->image=$request->input('image');
            $video->video_path=$request->input('video_path');
            $video->save();

            return response()->json(['message'=>'Register Video Success','code'=>200]);

        }catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Data Exist','code'=>400]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $video=Video::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid ID or No record Data','code'=>400]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $video=Video::findOrFail($id);
            $video->user_id=$request->input('user_id');
            $video->title=$request->input('title');
            $video->description=$request->input('description');
            $video->status=$request->input('status');
            $video->video_path=$request->input('video_path');
            $video->save();

            return response()->json(['message'=>'Register Updated Success','code'=>200]);

        }catch (\Exception $e) {
           return response()->json(['message'=>'Error Updated video Data','code'=>400]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $video=Video::findOrFail($id);
            $video->delete();
            return response()->json(['message'=>'Video Deleted Success','code'=>200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Not possible deleted Video ID','code'=>400]);
        }
    }
}
