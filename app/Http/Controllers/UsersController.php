<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=User::all();
        return response()->json($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'role'=>'required|max:7',
            'name'=>'required|max:70',
            'surname'=>'required|max:70',
            'email'=>'email|required',
            'password'=>'required|confirmed',
            'image'=>'required'
            ]);

        $validateData['password']=bcrypt($request->password);

        try {
            $user=new User;

            $image=$request->file('image');
            $path=$image->store('public');

            $user->role=$request->input('role');
            $user->name=$request->input('name');
            $user->surname=$request->input('surname');
            $user->email=$request->input('email');
            $user->password=bcrypt($request->input('password'));
            $user->image=$image;
            $user->remember_token=$request['remember_token']=Str::random(10);
            
            $user->save();

            return response()->json(['message'=>'Register User Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Data Exist','code'=>400]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $user=User::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid ID or No record Data','code'=>400]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user=User::findOrFail($id);
            $user->role=$request->input('role');
            $user->name=$request->input('name');
            $user->surname=$request->input('surname');
            $user->email=$request->input('email');
            $user->password=bcrypt($request->input('password'));
            $user->image=$request->input('image');
            $user->save();
            return response()->json(['message'=>'Register Updated Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Error Updated User Data','code'=>400]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user=User::findOrFail($id);
            $user->delete();
            return response()->json(['message'=>'Register Deleted Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Not possible deleted User Id','code'=>400]);
        }
    }
}