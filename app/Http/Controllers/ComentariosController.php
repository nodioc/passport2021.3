<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Comentario;



class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comentario=Comentario::all();
        return response()->json($comentario);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'user_id'=>'required|max:6',
            'video_id'=>'required|max:6',
            'body'=>'required|max:100',
        ]);   
        try{
            $comentario=new Comentario;
            $comentario->user_id=$request->input('user_id');
            $comentario->video_id=$request->input('video_id');
            $comentario->body=$request->input('body');
            $comentario->save();

            return response()->json(['message'=>'Register Commennt Success','code'=>200]);

        }catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Data Exist','code'=>400]);
       }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $comentario=Comentario::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid ID or No record Data','code'=>400]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $comentario=Comentario::findorFail($id);
            $comentario->user_id=$request->input('user_id');
            $comentario->video=$request->input('video_id');
            $comentario->body=$request->input('body');
            $comentario->save();
            return response()->json(['message'=>'Register Updated Success','code'=>200]);

        }catch (\Exception $e) {
            return response()->json(['message'=>'Error Comentario video Data','code'=>400]);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $comentario=Comentario::findOrFail($id);
            $comentario->delete();
            return response()->json(['message'=>'Comentario Deleted Success','code'=>200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Not possible deleted Comentario ID','code'=>400]);
        }
    }
}
