<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    public function comentarios(){
        return $this->hasMany('App\Comentarios');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario');
    }
     /**
     * The attributes that are mass assignable.
     *'role',
       
     * @var array
     */
    protected $fillable = [
       'user_id',
       'title',
       'description',
       'status',
       'image',
       'video_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
       
    ];
}

